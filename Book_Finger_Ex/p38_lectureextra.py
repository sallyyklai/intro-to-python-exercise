#compare the difference between n-r method and Heron method for solving square find square root of x
def nrsqrt(x,epsilon = 0.01):
    numguess = 1
    guess = x/2.0
    while abs(guess**2 - x) >= epsilon:
        guess = guess - ((guess**2 - x)/(2*guess))
        numguess += 1
    print('square root of',x,'is about',guess)
    print('number of guess n-r =',numguess)

def heronsqrt(x,epsilon = 0.01):
    numguess = 1
    guess = x/2.0
    while abs(guess**2 - x) >= epsilon:
        guess = (guess + x/guess)/2.0
        numguess += 1
    print('square root of',x,'is about',guess)
    print('number of guess using heron =',numguess)

def bisecsqrt(x,epsilon = 0.01):
    numguess = 1
    low = 0.0
    high = max(1.0,x)
    ans = (high + low)/2.0
    while abs(ans**2 - x) >= epsilon:
        #print('low = ',low,'high =', high,'ans = ', ans)
        numguess += 1
        if ans**2 < x:
            low = ans
        else:
            high = ans
        ans = (high + low)/2.0
    print ('number of guess using bisectional =',numguess)
    print(ans,'is close to square root of',x)

if __name__ == '__main__':
    nrsqrt(512)
    heronsqrt(512)
    bisecsqrt(512)
