def fib(n):
    """Assume n is int >= 0
    return Fibonacci of n"""
    if n==0 or n==1:
        return 1
    else:
        return fib(n-1) + fib(n-2)
