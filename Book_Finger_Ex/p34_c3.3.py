#!/anaconda3/bin/python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct  1 15:05:30 2018

@author: kwany
"""

#p34 findcuberoot using bisectional method
x = int(input('please input the number that you want to find cube root:'))
epsilon = 0.01
numguess = 1
low = min(x,-1)
high = max(1,x)
ans = (high + low)/2.0
while abs(ans**3 - x) >= epsilon:
    #print('low = ',low,'high =', high,'ans = ', ans)
    numguess += 1
    if ans**3 < x:
        low = ans
    else:
        high = ans
    ans = (high + low)/2.0
print ('number of guess =',numguess)
print(ans,'is close to cube root of',x )


# if __name__ == '__main__':
#     findcuberoot(-1e-6,epsilon = 0.00000001)
