def getratio(vect1,vect2):
    ratios = []
    for i,v in enumerate(vect1):
        try: ratios.append(vect1[i]/vect2[i])
        except ZeroDivisionError: ratios.append(float('nan'))
        # except IndexError: raise IndexError('call with bad argument')
        except: raise ValueError('call with bad argument')
    return ratios
if __name__ == '__main__':
    # print(getratio([1,2,3,4],[1,2,3,4]))
    try:
        print(getratio([],[]))
        print(getratio([1,2],[3]))
    except ValueError as msg: print(msg)
    # print(getratio([1,2],[3]))
