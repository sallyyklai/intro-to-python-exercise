def findandeven(L):
    if len([i for i in L if i%2==0]) >0:
        return [i for i in L if i%2==0][0]
    else:
        raise ValueError('list does not contain even number')

if __name__ == '__main__':
    findandeven([3,5,7,9,11])
