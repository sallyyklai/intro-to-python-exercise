#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct  1 15:07:40 2018

@author: kwany
"""

#p30 #s is a string contains different decimals
def sumstring(s):
    summing = 0
    x  = ''
    for i in s:
        if i != ',':
            x += i
        else:
            summing += float(x)
            x = ''
    summing += float(x)
    print(summing)

if __name__ == '__main__':
    sumstring('3.88888,9,1.888888')
