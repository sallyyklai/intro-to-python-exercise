#!/anaconda3/bin/python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct  1 15:06:29 2018

@author: kwany
"""

#p38 consider n-r method soving cube root compare with bisection search
def newtonraphson(x,epsilon = 0.01):
    numguess = 1
    guess = x/2.0
    while abs(guess**3 - x) >= epsilon:
        guess = guess - ((guess**3 - x)/(3*guess**2))
        numguess += 1
    print('cube root of',x,'is about',guess)
    print('number of guess =',numguess)

if __name__ == '__main__':
    newtonraphson(-1e-6,epsilon = 0.00000001)
