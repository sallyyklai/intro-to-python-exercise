class rational(object):
    def __init__(self,numer,denom):
        self.numer = numer
        self.denom = denom
    def __str__(self):
        return str(self.numer) + "/" + str(self.denom)
    def __add__(self,other):
        """basic addition"""
        numer = self.numer*other.denom + other.numer*self.denom
        denom = self.denom*other.denom
        return rational(numer,denom)
    def __sub__(self,other):
        """basic subtraction"""
        numer = self.numer*other.denom - other.numer*self.denom
        denom = self.denom*other.denom
        return rational(numer,denom)
    def __mul__(self,other):
        """basic multiplication"""
        numer = self.numer*other.numer
        denom = self.denom*other.denom
        return rational(numer,denom)
    def inverse(self):
        """flip the numer and denom"""
        return rational(self.denom,self.numer)
    def __truediv__(self,other):
        """basic division"""
        # numer = self.numer*other.denom
        # denom = self.denom*other.numer
        # return rational(numer,denom)
        return self*other.inverse()
    def __lt__(self,other):
        """smaller than comparison"""
        # return any([((other - self).numer > 0 and (other - self).denom > 0),((other - self).numer < 0 and (other - self).denom < 0)])
        return not(self >= other)
    def __le__(self,other):
        """smaller or equal to comparison"""
        return (other - self).numer >= 0
    def __gt__(self,other):
        """larger than comparison"""
        return not(self <= other)
    def __ge__(self,other):
        """"larger or equal to comparison"""
        return (other - self).numer <= 0
    def __eq__(self,other):
        """equal to comparison"""
        return (self.numer/self.denom == other.numer/other.denom)
    def __ne__(self,other):
        """not equal to comparison"""
        return not(self==other)
if __name__ == '__main__':
    half = rational(1,2)
    half2 = rational(2,4)
    third = rational(1,3)
    print(half == half2)
    print(half != third)
