# Problem Set 2, hangman.py
# Name:
# Collaborators:
# Time spent:

# Hangman Game
# -----------------------------------
# Helper code
# You don't need to understand this helper code,
# but you will have to know how to use the functions
# (so be sure to read the docstrings!)
import random
import string

WORDLIST_FILENAME = "words.txt"


def load_words():
    """
    Returns a list of valid words. Words are strings of lowercase letters.

    Depending on the size of the word list, this function may
    take a while to finish.
    """
    print("Loading word list from file...")
    # inFile: file
    inFile = open(WORDLIST_FILENAME, 'r')
    # line: string
    line = inFile.readline()
    # wordlist: list of strings
    wordlist = line.split()
    print("  ", len(wordlist), "words loaded.")
    return wordlist



def choose_word(wordlist):
    """
    wordlist (list): list of words (strings)

    Returns a word from wordlist at random
    """
    return random.choice(wordlist)

# end of helper code

# -----------------------------------

# Load the list of words into the variable wordlist
# so that it can be accessed from anywhere in the program
wordlist = load_words()


def is_word_guessed(secret_word, letters_guessed):
    '''
    secret_word: string, the word the user is guessing; assumes all letters are
      lowercase
    letters_guessed: list (of letters), which letters have been guessed so far;
      assumes that all letters are lowercase
    returns: boolean, True if all the letters of secret_word are in letters_guessed;
      False otherwise
    '''
    truelist = len(secret_word)*[False]
    if letters_guessed == []:
        return False
    else:
        for i,v in enumerate(secret_word):
            truelist[i] = (v in letters_guessed)
    return all(truelist)



def get_guessed_word(secret_word, letters_guessed):
    '''
    secret_word: string, the word the user is guessing
    letters_guessed: list (of letters), which letters have been guessed so far
    returns: string, comprised of letters, underscores (_), and spaces that represents
      which letters in secret_word have been guessed so far.
    '''
    outputlist = len(secret_word)*['_ ']
    for l in letters_guessed:
        if l in secret_word and secret_word.count(l)==1:
            outputlist[secret_word.index(l)] = l
        elif secret_word.count(l) > 1:
            for ii in [i for i, v in enumerate(secret_word) if v == l]:
                outputlist[ii] = l
    return ''.join(outputlist)


def get_available_letters(letters_guessed):
    '''
    letters_guessed: list (of letters), which letters have been guessed so far
    returns: string (of letters), comprised of letters that represents which letters have not
      yet been guessed.
    '''
    letterlist = [v for i,v in enumerate(string.ascii_lowercase)]
    for l in letters_guessed:
        letterlist.remove(l)
    return ''.join(letterlist)



def hangman(secret_word):
    '''
    secret_word: string, the secret word to guess.

    Starts up an interactive game of Hangman.

    * At the start of the game, let the user know how many
      letters the secret_word contains and how many guesses s/he starts with.

    * The user should start with 6 guesses

    * Before each round, you should display to the user how many guesses
      s/he has left and the letters that the user has not yet guessed.

    * Ask the user to supply one guess per round. Remember to make
      sure that the user puts in a letter!

    * The user should receive feedback immediately after each guess
      about whether their guess appears in the computer's word.

    * After each guess, you should display to the user the
      partially guessed word so far.

    Follows the other limitations detailed in the problem write-up.
    '''
    num_guess_left = 6
    letters_guessed = []
    num_warning_left = 3
    consonants = 'bcdfghjklmnpqrstwxyz'
    vowels = 'aeiou'
    print('Welcome to the game Hangman!')
    print('I am thinking of a word that is', len(secret_word), 'letters long')
    print("You have",num_warning_left,'warnings left')
    while num_guess_left > 0 and (not is_word_guessed(secret_word, letters_guessed)):
        print('-------------')
        print('You have', num_guess_left, 'guesses left')
        print('Available letters:',get_available_letters(letters_guessed))
        singleguess = input('Please guess a letter: ')
        if (not(singleguess in string.ascii_lowercase)) and (not(singleguess in string.ascii_uppercase)):
            if num_warning_left == 0:
                print("Oops! That's not a valid letter. You have no warning left and you lose a guess:", get_guessed_word(secret_word, letters_guessed))
                num_guess_left -= 1
            else:
                num_warning_left -= 1
                print("Oops! That's not a valid letter. You have",num_warning_left,"warnings left:",get_guessed_word(secret_word, letters_guessed))
        elif singleguess in letters_guessed:
            if num_warning_left == 0:
                print("Oops, You've already guessed that letter. You have no warning left and you lose a guess:", get_guessed_word(secret_word, letters_guessed))
                num_guess_left -= 1
            else:
                num_warning_left -= 1
                print("Oops, You've already guessed that letter. You have",num_warning_left,'warnings left:',get_guessed_word(secret_word, letters_guessed))
        else:
            letters_guessed.append(singleguess)
            if singleguess in secret_word:
                print('Good guess:',get_guessed_word(secret_word, letters_guessed))
                if is_word_guessed(secret_word, letters_guessed):
                    print('-------------')
                    print('Congratulations, you won!')
                    print('Your score is',num_guess_left*len(set(secret_word)))
            else:
                print('Oops! that letter is not in my word: ', get_guessed_word(secret_word, letters_guessed))
                if singleguess in consonants:
                    num_guess_left -= 1
                elif singleguess in vowels:
                    num_guess_left -= 2
    if num_guess_left < 0:
        print('-------------')
        print('Sorry, you ran out of guesses. The word is:', secret_word)





def match_with_gaps(my_word, other_word):
    '''
    my_word: string with _ characters, current guess of secret word
    other_word: string, regular English word
    returns: boolean, True if all the actual letters of my_word match the
        corresponding letters of other_word, or the letter is the special symbol
        _ , and my_word and other_word are of the same length;
        False otherwise:
    '''
    input = my_word.replace(" ", "")
    truelist = len(input)*[False]
    if len(input) != len(other_word): return False
    else:
        for i,v in enumerate(input):
            if v == '_' and not(other_word[i] in input): truelist[i] = True
            if v == other_word[i]: truelist[i] = True
    return all(truelist)



def show_possible_matches(my_word):
    '''
    my_word: string with _ characters, current guess of secret word
    returns: nothing, but should print out every word in wordlist that matches my_word
             Keep in mind that in hangman when a letter is guessed, all the positions
             at which that letter occurs in the secret word are revealed.
             Therefore, the hidden letter(_ ) cannot be one of the letters in the word
             that has already been revealed.

    '''
    truelist = len(wordlist)*[False]
    matchlist = []
    for i,v in enumerate(wordlist):
        if match_with_gaps(my_word,v):
            truelist[i] = True
            matchlist.append(v)
    if not any(truelist): print('No matches found')
    else:
        print(",".join(matchlist).replace(",", " "))



def hangman_with_hints(secret_word):
    '''
    secret_word: string, the secret word to guess.

    Starts up an interactive game of Hangman.

    * At the start of the game, let the user know how many
      letters the secret_word contains and how many guesses s/he starts with.

    * The user should start with 6 guesses

    * Before each round, you should display to the user how many guesses
      s/he has left and the letters that the user has not yet guessed.

    * Ask the user to supply one guess per round. Make sure to check that the user guesses a letter

    * The user should receive feedback immediately after each guess
      about whether their guess appears in the computer's word.

    * After each guess, you should display to the user the
      partially guessed word so far.

    * If the guess is the symbol *, print out all words in wordlist that
      matches the current guessed word.

    Follows the other limitations detailed in the problem write-up.
    '''
    num_guess_left = 6
    letters_guessed = []
    num_warning_left = 3
    consonants = 'bcdfghjklmnpqrstwxyz'
    vowels = 'aeiou'
    print('Welcome to the game Hangman!')
    print('I am thinking of a word that is', len(secret_word), 'letters long')
    print("You have",num_warning_left,'warnings left')
    while num_guess_left > 0 and (not is_word_guessed(secret_word, letters_guessed)):
        print('-------------')
        print('You have', num_guess_left, 'guesses left')
        print('Available letters:',get_available_letters(letters_guessed))
        singleguess = input('Please guess a letter: ')
        if (not(singleguess in string.ascii_lowercase)) and (not(singleguess in string.ascii_uppercase)) and (singleguess != '*'):
            if num_warning_left == 0:
                print("Oops! That's not a valid letter. You have no warning left and you lose a guess:", get_guessed_word(secret_word, letters_guessed))
                num_guess_left -= 1
            else:
                num_warning_left -= 1
                print("Oops! That's not a valid letter. You have",num_warning_left,"warnings left:",get_guessed_word(secret_word, letters_guessed))
        elif singleguess in letters_guessed:
            if num_warning_left == 0:
                print("Oops, You've already guessed that letter. You have no warning left and you lose a guess:", get_guessed_word(secret_word, letters_guessed))
                num_guess_left -= 1
            else:
                num_warning_left -= 1
                print("Oops, You've already guessed that letter. You have",num_warning_left,'warnings left:',get_guessed_word(secret_word, letters_guessed))
        elif singleguess == '*':
            print('Possible word matches are: ')
            show_possible_matches(get_guessed_word(secret_word,letters_guessed))
        else:
            letters_guessed.append(singleguess)
            if singleguess in secret_word:
                print('Good guess:',get_guessed_word(secret_word, letters_guessed))
                if is_word_guessed(secret_word, letters_guessed):
                    print('-------------')
                    print('Congratulations, you won!')
                    print('Your score is',num_guess_left*len(set(secret_word)))
            else:
                print('Oops! that letter is not in my word: ', get_guessed_word(secret_word, letters_guessed))
                if singleguess in consonants:
                    num_guess_left -= 1
                elif singleguess in vowels:
                    num_guess_left -= 2
    if num_guess_left < 0:
        print('-------------')
        print('Sorry, you ran out of guesses. The word is:', secret_word)



# When you've completed your hangman_with_hint function, comment the two similar
# lines above that were used to run the hangman function, and then uncomment
# these two lines and run this file to test!
# Hint: You might want to pick your own secret_word while you're testing.


# if __name__ == "__main__":
    #secret_word = choose_word(wordlist)
    #hangman('abcdefgh')

    # To test part 3 re-comment out the above lines and
    # uncomment the following two lines.

    #secret_word = choose_word(wordlist)
    #hangman_with_hints(secret_word)
