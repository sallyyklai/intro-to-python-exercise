# Problem Set 4A
# Name: <your name here>
# Collaborators:
# Time Spent: x:xx

def get_permutations(sequence):
    '''
    Enumerate all permutations of a given string

    sequence (string): an arbitrary string to permute. Assume that it is a
    non-empty string.

    You MUST use recursion for this part. Non-recursive solutions will not be
    accepted.

    Returns: a list of all permutations of sequence

    Example:
    >>> get_permutations('abc')
    ['abc', 'acb', 'bac', 'bca', 'cab', 'cba']

    Note: depending on your implementation, you may return the permutations in
    a different order than what is listed here.
    '''
    if len(sequence) == 1:
        return [sequence]
    else:
        first_letter = sequence[0]
        rest_of_the_word = sequence[1:]
        return insert_letter_into_word_list(first_letter,get_permutations(rest_of_the_word))

def insert_letter_into_word_list(letter,listofword):
    """return list of combination by inserting letter in to every spaces in every word in listofword"""
    final = []
    for word in listofword:
        final += insert_letter_into_word(letter,word)
    return list(set(final))

def insert_letter_into_word(letter,word):
    """Assume both are strings, return a list of words created by inserting the letter into all places of the word"""
    result = [letter + word, word + letter]
    for i in range(1,len(word)):
        result.append(word[:i] + letter + word[i:])
    return result


if __name__ == '__main__':
    # from math import factorial
    # print(len(get_permutations('abcdefg')) == factorial(7))
    print(get_permutations('abc'))
    # print(get_permutations('ykl'))
    # print(get_permutations('lsh'))
    # print(get_permutations('aab'))
