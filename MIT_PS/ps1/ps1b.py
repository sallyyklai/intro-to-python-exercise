annual_salary = float(input('Enter your annual salary:'))
portion_saved = float(input('Enter the percent of your salary to save, as decimal:'))
total_cost = float(input('Enter the cost of your dream:'))
semi_annual_raise = float(input('Enter your semi-annual salary raise:'))
portion_down_payment = 0.25*total_cost
current_savings = 0.0
month_save = 0
while current_savings < portion_down_payment:
    month_save += 1
    if month_save > 1 and (month_save-1)%6 == 0:
        annual_salary *= (1+semi_annual_raise)
    current_savings += (portion_saved*(annual_salary/12) + current_savings*0.04/12)

print('Number of months:',month_save)
