
# #investment_return = 0.04
def savingafternmonth(annual_salary,months,portion_saved):
    current_savings = 0.0
    semi_annual_raise = 0.07
    for i in range(1,months+1):
        if i>1 and (i-1)%6 == 0:
            annual_salary *= (1+semi_annual_raise)
        current_savings += ((portion_saved/100)*(annual_salary/12) + current_savings*0.04/12)
    return current_savings

def findsavingpercentage(annual_salary):
    portion_down_payment = 0.25*1e6
    percentage_low = 0
    percentage_high = 100.00
    portion_saved = (percentage_low + percentage_high)/2.0
    count = 0
    if savingafternmonth(annual_salary,36,100) < portion_down_payment:
        return 'Not possible even save at 100%'
    else:
        while abs(savingafternmonth(annual_salary,36,portion_saved) - portion_down_payment) > 100:
            count += 1
            if savingafternmonth(annual_salary,36,portion_saved) - portion_down_payment > 100:
                percentage_high = portion_saved
                portion_saved = (percentage_low + percentage_high)/2.0
            else:
                percentage_low = portion_saved
                portion_saved = (percentage_low + percentage_high)/2.0
        return 'saving rate is', portion_saved/100, 'steps of bisection search is', count
