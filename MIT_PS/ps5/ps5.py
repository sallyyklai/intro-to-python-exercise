# 6.0001/6.00 Problem Set 5 - RSS Feed Filter
# Name:
# Collaborators:
# Time:

import feedparser
import string
import time
import threading
from project_util import translate_html
from mtTkinter import *
from datetime import datetime
import pytz


#-----------------------------------------------------------------------

#======================
# Code for retrieving and parsing
# Google and Yahoo News feeds
# Do not change this code
#======================

def process(url):
    """
    Fetches news items from the rss url and parses them.
    Returns a list of NewsStory-s.
    """
    feed = feedparser.parse(url)
    entries = feed.entries
    ret = []
    for entry in entries:
        guid = entry.guid
        title = translate_html(entry.title)
        link = entry.link
        description = translate_html(entry.description)
        pubdate = translate_html(entry.published)

        try:
            pubdate = datetime.strptime(pubdate, "%a, %d %b %Y %H:%M:%S %Z")
            pubdate.replace(tzinfo=pytz.timezone("GMT"))
          #  pubdate = pubdate.astimezone(pytz.timezone('EST'))
          #  pubdate.replace(tzinfo=None)
        except ValueError:
            pubdate = datetime.strptime(pubdate, "%a, %d %b %Y %H:%M:%S %z")

        newsStory = NewsStory(guid, title, description, link, pubdate)
        ret.append(newsStory)
    return ret

#======================
# Data structure design
#======================

# Problem 1
class NewsStory(object):
    def __init__(self, guid, title, description, link, pubdate):
        self.guid = guid
        self.title = title
        self.description = description
        self.link = link
        self.pubdate = pubdate

    def get_guid(self):
        return self.guid

    def get_title(self):
        return self.title

    def get_description(self):
        return self.description

    def get_link(self):
        return self.link

    def get_pubdate(self):
        return self.pubdate

#======================
# Triggers
#======================

class Trigger(object):
    def evaluate(self, story):
        """
        Returns True if an alert should be generated
        for the given news item, or False otherwise.
        """
        # DO NOT CHANGE THIS!
        raise NotImplementedError

# PHRASE TRIGGERS

# Problem 2
class PhraseTrigger(Trigger):
    def __init__(self, phrase):
        self.phrase = phrase.lower()

    def is_phrase_in(self, grand_text):
        for puns in string.punctuation:
            if puns in grand_text:
                grand_text = grand_text.replace(puns," ")
        tidy_string_list = grand_text.lower().split()
        # print(tidy_string_list)
        return all([(c in tidy_string_list) for c in self.phrase.split()]) and (self.phrase in " ".join(grand_text.lower().split()))

# Problem 3
class TitleTrigger(PhraseTrigger):
    def __init__(self, title):
        PhraseTrigger.__init__(self,title)
    def evaluate(self, story):
        return self.is_phrase_in(story.title)

# Problem 4
class DescriptionTrigger(PhraseTrigger):
    def __init__(self, title):
        PhraseTrigger.__init__(self,title)
    def evaluate(self, story):
        return self.is_phrase_in(story.description)

# TIME TRIGGERS

# Problem 5
class TimeTrigger(Trigger):
    def __init__(self,stringtime):
        self.pubtime = datetime.strptime(stringtime,"%d %b %Y %H:%M:%S")
# Constructor:
#        Input: Time has to be in EST and in the format of "%d %b %Y %H:%M:%S".
#        Convert time from string to a datetime before saving it as an attribute.

# Problem 6
class BeforeTrigger(TimeTrigger):
    def __init__(self,stringtime):
        TimeTrigger.__init__(self,stringtime)
    def evaluate(self,story):
        return story.pubdate.replace(tzinfo=pytz.timezone("EST")) < self.pubtime.replace(tzinfo=pytz.timezone("EST"))

class AfterTrigger(TimeTrigger):
    def __init__(self,stringtime):
        TimeTrigger.__init__(self,stringtime)
    def evaluate(self,story):
        return story.pubdate.replace(tzinfo=pytz.timezone("EST")) > self.pubtime.replace(tzinfo=pytz.timezone("EST"))

# COMPOSITE TRIGGERS

# Problem 7
class NotTrigger(Trigger):
    def __init__(self,trigger):
        self.trigger = trigger
    def evaluate(self,story):
        return not(self.trigger.evaluate(story))

# Problem 8
class AndTrigger(Trigger):
    def __init__(self,trig1,trig2):
        self.trig1 = trig1
        self.trig2 = trig2
    def evaluate(self,story):
        return self.trig1.evaluate(story) and self.trig2.evaluate(story)

# Problem 9
class OrTrigger(Trigger):
    def __init__(self,trig1,trig2):
        self.trig1 = trig1
        self.trig2 = trig2
    def evaluate(self,story):
        return self.trig1.evaluate(story) or self.trig2.evaluate(story)

#======================
# Filtering
#======================

# Problem 10
def filter_stories(stories, triggerlist):
    """
    Takes in a list of NewsStory instances.

    Returns: a list of only the stories for which a trigger in triggerlist fires.
    """
    # TODO: Problem 10
    filtered_stories_list = []
    for trigger in triggerlist:
        for story in stories:
            if trigger.evaluate(story):
                filtered_stories_list.append(story)
    # (we're just returning all the stories, with no filtering)
    return filtered_stories_list



#======================
# User-Specified Triggers
#======================
# Problem 11
def read_trigger_config(filename):
    """
    filename: the name of a trigger configuration file

    Returns: a list of trigger objects specified by the trigger configuration
        file.
    """
    # We give you the code to read in the file and eliminate blank lines and
    # comments. You don't need to know how it works for now!
    trig_list = []
    trigger_file = open(filename, 'r')
    lines = []
    for line in trigger_file:
        line = line.rstrip()
        if not (len(line) == 0 or line.startswith('//')):
            lines.append(line)
    # TODO: Problem 11
    trigdict = {}
    def correspond_trig_arg(op_string,list_of_arg):
        if op_string == 'TITLE':
            return TitleTrigger(list_of_arg[0])
        if op_string == 'DESCRIPTION':
            return DescriptionTrigger(list_of_arg[0])
        if op_string == 'AFTER':
            return AfterTrigger(list_of_arg[0])
        if op_string == 'BEFORE':
            return BeforeTrigger(list_of_arg[0])
        if op_string == 'NOT':
            return NotTrigger(trigdict[list_of_arg[0]])
        if op_string == 'AND':
            return AndTrigger(trigdict[list_of_arg[0]],trigdict[list_of_arg[1]])
        if op_string == 'OR':
            return OrTrigger(trigdict[list_of_arg[0]],trigdict[list_of_arg[1]])
    for definition in lines:
        seperated = definition.split(',')
        if seperated[0] != 'ADD':
            trigdict[seperated[0]] = correspond_trig_arg(seperated[1],seperated[2:])
        else:
            for trig in seperated[1:]:
                trig_list.append(trigdict[trig])
    print(trig_list)
    print(trigdict) # for now, print it so you see what it contains!
    return trig_list


SLEEPTIME = 120 #seconds -- how often we poll

def main_thread(master):
    # A sample trigger list - you might need to change the phrases to correspond
    # to what is currently in the news
    try:
        t1 = TitleTrigger("election")
        t2 = DescriptionTrigger("Trump")
        t3 = DescriptionTrigger("Clinton")
        t4 = AndTrigger(t2, t3)
        triggerlist = [t1, t4]

        # Problem 11
        # TODO: After implementing read_trigger_config, uncomment this line
        triggerlist = read_trigger_config('triggers.txt')

        # HELPER CODE - you don't need to understand this!
        # Draws the popup window that displays the filtered stories
        # Retrieves and filters the stories from the RSS feeds
        frame = Frame(master)
        frame.pack(side=BOTTOM)
        scrollbar = Scrollbar(master)
        scrollbar.pack(side=RIGHT,fill=Y)

        t = "Google & Yahoo Top News"
        title = StringVar()
        title.set(t)
        ttl = Label(master, textvariable=title, font=("Helvetica", 18))
        ttl.pack(side=TOP)
        cont = Text(master, font=("Helvetica",14), yscrollcommand=scrollbar.set)
        cont.pack(side=BOTTOM)
        cont.tag_config("title", justify='center')
        button = Button(frame, text="Exit", command=root.destroy)
        button.pack(side=BOTTOM)
        guidShown = []
        def get_cont(newstory):
            if newstory.get_guid() not in guidShown:
                cont.insert(END, newstory.get_title()+"\n", "title")
                cont.insert(END, "\n---------------------------------------------------------------\n", "title")
                cont.insert(END, newstory.get_description())
                cont.insert(END, "\n*********************************************************************\n", "title")
                guidShown.append(newstory.get_guid())

        while True:

            print("Polling . . .", end=' ')
            # Get stories from Google's Top Stories RSS news feed
            stories = process("http://news.google.com/news?output=rss")

            # Get stories from Yahoo's Top Stories RSS news feed
            stories.extend(process("http://news.yahoo.com/rss/topstories"))

            stories = filter_stories(stories, triggerlist)

            list(map(get_cont, stories))
            scrollbar.config(command=cont.yview)


            print("Sleeping...")
            time.sleep(SLEEPTIME)

    except Exception as e:
        print(e)


if __name__ == '__main__':
    root = Tk()
    root.title("Some RSS parser")
    t = threading.Thread(target=main_thread, args=(root,))
    t.start()
    root.mainloop()
