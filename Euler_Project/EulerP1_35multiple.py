#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct  1 14:30:29 2018

@author: kwany
"""

def summultipleof35(n=1000):
    summing = 0
    for i in range(1,n):
        if i%3==0 or i%5==0:
            summing += i
    print('sum of all the multiples of 3 or 5 below',n, '=',summing)
