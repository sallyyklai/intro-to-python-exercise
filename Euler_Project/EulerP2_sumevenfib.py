#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct  1 14:36:36 2018

@author: kwany
"""

fib = [1,2]
summing = 0
while fib[-1]+fib[-2] < 4e6:
    fib.append(fib[-1]+fib[-2])
for i in fib:
    if i%2 == 0:
        summing += i
print(summing)
