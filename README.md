# Python Exercises
This repo is used to store my python exercises, including:
* Finger exercises on Guttag's book *Introduction to Computation and Programming Using Python*.
* MIT problem sheets from the MIT [Introduction to Computer Science and Programming in Python](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-0001-introduction-to-computer-science-and-programming-in-python-fall-2016/index.htm) course.
  * Problem sheet 0: Basic set up
  * Problem sheet 1: House mortgage calculation problem
  * Problem sheet 2: Hangman game design
  * Problem sheet 3: Scrabble game design
  * Problem sheet 4: Encryption and decryption of message
  * Problem sheet 5: News alert system
* Problems in [Project Euler](https://projecteuler.net/archives).
